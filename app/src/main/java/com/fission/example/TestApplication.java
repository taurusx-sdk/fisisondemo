package com.fission.example;

import android.app.Application;
import android.util.Log;

import com.fission.account.AccountBuilder;
import com.fission.account.FissionAccounts;
import com.fission.account.InitCallback;
import com.fission.example.constance.Constance;
import com.we.modoo.ModooHelper;

public class TestApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initSDK();
    }

    private void initSDK() {
        AccountBuilder builder = new AccountBuilder.Builder()
                .setAppKey("Nxz2zsipG6")
                .setHostUrl("https://api-lovematch.freeqingnovel.com")
//                .setDeviceId("3361571ab3773f56")
//                .setDeviceId("60fe37e31ca34a17_eee595b23659d099995c682fd6b879d2") // S2AEZL
                .setDeviceId("test_zly_user_1")
//                .setDeviceId("zly_user_three_8")
//                .setDeviceId("fission_test_invitation")
//                .setDeviceId("fission_test_invitation_2")
                .setPlatformId("S150")
                .setChannel("test")
                .build();
        FissionAccounts.init(getApplicationContext(), builder, new InitCallback() {
            @Override
            public void onResult(int status) {
                Log.d(Constance.TAG, "Init result code is : " + status);
            }
        });
        FissionAccounts.setTestMode(true);
        ModooHelper.init(this);
    }
}
