package com.fission.example.login;

public interface LoginCallback {
    void onSuccess(UserInfo user);

    void onFailed(String msg);
}
