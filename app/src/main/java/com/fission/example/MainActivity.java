package com.fission.example;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.fission.account.FissionAccounts;
import com.fission.example.activity.BaseActivity;
import com.fission.example.activity.DataActivity;
import com.fission.example.activity.MissionActivity;
import com.fission.example.activity.ShareActivity;
import com.fission.example.activity.TongActivity;
import com.fission.example.activity.UserActivity;
import com.fission.example.activity.WithDrawActivity;
import com.fission.example.constance.Constance;
import com.fission.example.util.ToastUtil;
import com.fission.sdk.Fission;
import com.fission.sdk.FissionCallback;

import java.util.HashMap;
import java.util.Set;

public class MainActivity extends BaseActivity {
    private final String TAG = "FissionDemo";

    private Button mInitButton;
    private Button mUserButton;
    private Button mTongButton;
    private Button mShareButton;
    private Button mMissionButton;
    private Button mWithdrawButton;
    private Button mDataButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        register();
    }

    /**
     * 用户注册后会保存用户名信息，
     * 产品根据自身逻辑保存 UserBean 中相应信息
     */
    private void register() {
        if (TextUtils.isEmpty(FissionAccounts.getUserId())) {
            ToastUtil.showToast(MainActivity.this, "Please register first");
        }
    }


    private void initView() {
        mInitButton = findViewById(R.id.fission_demo_init);
        mInitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        mUserButton = findViewById(R.id.fission_demo_user);
        mUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, UserActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mTongButton = findViewById(R.id.fission_demo_tong);
        mTongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, TongActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mShareButton = findViewById(R.id.fission_demo_share);
        mShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, ShareActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mMissionButton = findViewById(R.id.fission_demo_mission);
        mMissionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, MissionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mWithdrawButton = findViewById(R.id.fission_demo_withdraw);
        mWithdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, WithDrawActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });


        mDataButton = findViewById(R.id.fission_demo_data);
        mDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, DataActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

}
