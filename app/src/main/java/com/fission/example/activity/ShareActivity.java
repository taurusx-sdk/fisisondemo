package com.fission.example.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.fission.account.FissionAccounts;
import com.fission.example.R;
import com.fission.example.constance.Constance;
import com.fission.sdk.Fission;
import com.fission.sdk.FissionCallback;

import java.util.HashMap;
import java.util.Set;

public class ShareActivity extends BaseActivity {
    private Button mGenScene;
    private Button mGenQRCode;
    private Button mGenQRCodeBytes;
    private ImageView mQRImageView;
    private Button mRestoreScene;

    private String mShareUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        init();
        initView();
    }

    private void init() {
        Fission.Share.init(ShareActivity.this);
    }

    private void initView() {
        mGenScene = findViewById(R.id.fission_demo_gen_scene);
        mGenScene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("uid", FissionAccounts.getUserId());
                Fission.Share.genShareUrl("http://share.msgcarry.cn/share/ceshiluodiye.html", map, new FissionCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        mShareUrl = result;
                        Log.d(Constance.TAG, "the result is " + result);

                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(Constance.TAG, "the code is " + code + " the msg is " + msg);
                    }
                });
            }
        });

        mGenQRCodeBytes = findViewById(R.id.fission_demo_gen_qr_bytes);
        mGenQRCodeBytes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShareUrl != null) {
                    byte[] bitmap = Fission.Share.getQRCodeBytes(mShareUrl, 200, 200);
                    if (mQRImageView != null) {
                        mQRImageView.setImageBitmap(BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length));
                    }
                }
            }
        });

        mGenQRCode = findViewById(R.id.fission_demo_gen_qr);
        mGenQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShareUrl != null) {
                    Bitmap bitmap = Fission.Share.getQRCodeBitmap(mShareUrl, 200, 200);
                    if (mQRImageView != null) {
                        mQRImageView.setImageBitmap(bitmap);
                    }
                }
            }
        });
        mQRImageView = findViewById(R.id.fission_demo_qr_view);

        mRestoreScene = findViewById(R.id.fission_demo_restore_scene);
        mRestoreScene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Share.getInstallParams(new FissionCallback<HashMap<String, Object>>() {
                    @Override
                    public void onSuccess(HashMap<String, Object> params) {
                        if (params != null) {
                            Log.d(Constance.TAG, "get the params:");
                            Set<String> keys = params.keySet();
                            for (String key : keys) {
                                Log.d(Constance.TAG, "the key is" + key + " the value is " + params.get(key));
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(Constance.TAG, "the code is " + code + " the msg is " + msg);
                    }
                });
            }
        });
    }
}
