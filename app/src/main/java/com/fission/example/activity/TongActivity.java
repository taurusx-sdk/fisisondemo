package com.fission.example.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.fission.example.R;
import com.fission.example.constance.Constance;
import com.fission.example.util.ToastUtil;
import com.fission.sdk.Fission;
import com.fission.sdk.FissionCallback;
import com.fission.sdk.bean.sect.ApprenticeInfo;
import com.fission.sdk.bean.sect.ApprenticeList;
import com.fission.sdk.bean.sect.AwardInfo;
import com.fission.sdk.bean.sect.Contribution;
import com.fission.sdk.bean.sect.ContributionRecord;
import com.fission.sdk.bean.sect.RedPacketRecords;
import com.fission.sdk.bean.sect.SectInfo;
import com.fission.sdk.bean.sect.SectSettings;
import com.fission.sdk.bean.sect.TransformInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TongActivity extends BaseActivity {
    private static final String TAG = Constance.TAG;

    private final String TONG_ID = "0532f4e1c5bd385d";
    private final String APPRENTICE_ID = "a7abc96eb6ac454d";
    private ArrayList<ApprenticeInfo> mStudentList;
    private final String TONG_MISSION_ID = "";
    // data
    private static Button mGetTongInfo;
    private static Button mGetStudents;
    private static Button mGetStudentDetail;
    private static Button mGenStar;
    private static Button mGetStar;
    private static Button mGetStarAll;
    private static Button mGetInviteReward;
    private static Button mGetInviteListConfig;
    private static Button mBindInviter;
    private static Button mGetStarDayRecord;
    private static Button mGetRedPacketRecord;
    private static Button mTransform;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tong);
        initView();
    }

    private void initView() {
        mGetTongInfo = findViewById(R.id.fission_tong_get_tong_info);
        mGetTongInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.getSectInfo(new FissionCallback<SectInfo>() {
                    @Override
                    public void onSuccess(SectInfo tongInfo) {
                        Log.d(TAG, tongInfo.toString());
                        ToastUtil.showToast(TongActivity.this, "获取宗门信息成功");
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is: " + code + " msg : " + msg);
                        ToastUtil.showToast(TongActivity.this, "获取宗门失败： " + msg);
                    }
                });
            }
        });

        mGetStudents = findViewById(R.id.fission_tong_get_students);
        mGetStudents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.getApprenticeList(1, -1, -1, new FissionCallback<ApprenticeList>() {
                    @Override
                    public void onSuccess(ApprenticeList list) {
                        Log.d(TAG, list.toString());
                        ToastUtil.showToast(TongActivity.this, "获取徒弟信息成功");

                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is: " + code + " msg : " + msg);
                        ToastUtil.showToast(TongActivity.this, "获取徒弟信息失败： " + msg);
                    }
                });
            }
        });

        mGetStudentDetail = findViewById(R.id.fission_tong_get_student_detail);
        mGetStudentDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.getApprenticeInfo(APPRENTICE_ID, new FissionCallback<ApprenticeInfo>() {
                    @Override
                    public void onSuccess(ApprenticeInfo student) {
                        if (student != null) {
                            Log.d(TAG, student.toString());
                            ToastUtil.showToast(TongActivity.this, "获取具体徒弟信息成功");
                        } else {
                            Log.d(TAG, "student is null");
                            ToastUtil.showToast(TongActivity.this, "获取具体徒弟信息为空");
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                        ToastUtil.showToast(TongActivity.this, "获取具体徒弟信息失败： " + msg);
                    }
                });
            }
        });


        mGenStar = findViewById(R.id.fission_tong_gen_star);
        mGenStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.genContribution(0, new FissionCallback<Contribution>() {
                    @Override
                    public void onSuccess(Contribution contribution) {
                        if (contribution != null) {
                            if (contribution != null) {
                                Log.d(TAG, contribution.toString());
                                ToastUtil.showToast(TongActivity.this, "生成贡献成功");
                            } else {
                                Log.d(TAG, "student is null");
                                ToastUtil.showToast(TongActivity.this, "生成贡献信息为空");
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                        ToastUtil.showToast(TongActivity.this, "生成贡献失败： " + msg);
                    }
                });

            }
        });


        mGetStar = findViewById(R.id.fission_tong_get_star);
        mGetStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.getContribution("", new FissionCallback<Contribution>() {
                    @Override
                    public void onSuccess(Contribution contribution) {
                        if (contribution != null) {
                            Log.d(TAG, contribution.toString());
                            ToastUtil.showToast(TongActivity.this, "领取贡献成功");
                        } else {
                            Log.d(TAG, "student is null");
                            ToastUtil.showToast(TongActivity.this, "领取贡献信息为空");
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                        ToastUtil.showToast(TongActivity.this, "领取贡献失败： " + msg);
                    }
                });
            }
        });

        mGetStarAll = findViewById(R.id.fission_tong_get_star_all);
        mGetStarAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.getAllContribution(new FissionCallback<Contribution>() {
                    @Override
                    public void onSuccess(Contribution contribution) {
                        if (contribution != null) {
                            Log.d(TAG, contribution.toString());
                            ToastUtil.showToast(TongActivity.this, "领取贡献成功");
                        } else {
                            Log.d(TAG, "student is null");
                            ToastUtil.showToast(TongActivity.this, "领取贡献信息为空");
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                        ToastUtil.showToast(TongActivity.this, "领取贡献失败： " + msg);
                    }
                });
            }
        });

        mGetInviteListConfig = findViewById(R.id.fission_tong_get_invite_config_list);
        mGetInviteListConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.getInviteAwardList(new FissionCallback<List<SectSettings.InviteAward>>() {
                    @Override
                    public void onSuccess(List<SectSettings.InviteAward> list) {
                        if (list != null && list.size() > 0) {
                            ToastUtil.showToast(TongActivity.this, "领取邀请配置成功");
                            for (SectSettings.InviteAward item : list) {
                                Log.d(TAG, item.toString());
                            }
                        } else {
                            Log.d(TAG, "student is null");
                            ToastUtil.showToast(TongActivity.this, "领取邀请配置为空");
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                        ToastUtil.showToast(TongActivity.this, "领取邀请配置失败： " + msg);
                    }
                });
            }
        });

        mGetInviteReward = findViewById(R.id.fission_tong_get_invite_award);
        mGetInviteReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.getInviteAward(1, new FissionCallback<AwardInfo>() {
                    @Override
                    public void onSuccess(AwardInfo awardInfo) {
                        if (awardInfo != null) {
                            Log.d(TAG, awardInfo.toString());
                            ToastUtil.showToast(TongActivity.this, "领取邀请奖励成功");
                        } else {
                            Log.d(TAG, "student is null");
                            ToastUtil.showToast(TongActivity.this, "领取邀请奖励为空");
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                        ToastUtil.showToast(TongActivity.this, "领取邀请奖励失败： " + msg);
                    }
                });
            }
        });

        mBindInviter = findViewById(R.id.fission_tong_bind_inviter);
        mBindInviter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.bindInviter(TONG_ID, new FissionCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean success) {
                        if (success) {
                            ToastUtil.showToast(TongActivity.this, "绑定成功");
                        } else {
                            ToastUtil.showToast(TongActivity.this, "绑定失败");
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                        ToastUtil.showToast(TongActivity.this, "绑定失败： " + msg);
                    }
                });
            }
        });

        mGetStarDayRecord = findViewById(R.id.fission_tong_get_star_day_record);
        mGetStarDayRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.getContributionRecordByDay(2021, 1, new FissionCallback<Map<String, ContributionRecord>>() {
                    @Override
                    public void onSuccess(Map<String, ContributionRecord> recordsMap) {
                        Log.d(TAG, "the list is :");
                        if (recordsMap != null) {
                            Set<String> keys = recordsMap.keySet();
                            for (String date : keys) {
                                Log.d(TAG, "date : " + date + " , " + recordsMap.get(date).toString());
                            }
                        }
                        ToastUtil.showToast(TongActivity.this, "获取贡献记录成功");
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                        ToastUtil.showToast(TongActivity.this, "获取贡献记录失败： " + msg);
                    }
                });
            }
        });

        mGetRedPacketRecord = findViewById(R.id.fission_tong_get_red_packet_record);
        mGetRedPacketRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.getRedPacketRecord(new FissionCallback<RedPacketRecords>() {
                    @Override
                    public void onSuccess(RedPacketRecords packetRecords) {
                        if (packetRecords != null) {
                            ToastUtil.showToast(TongActivity.this, "获取红包记录成功");
                            Log.d(TAG, packetRecords.toString());
                        } else {
                            ToastUtil.showToast(TongActivity.this, "获取红包记录为空");
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                        ToastUtil.showToast(TongActivity.this, "获取红包记录失败： " + msg);
                    }
                });
            }
        });

        mTransform = findViewById(R.id.fission_tong_transform);
        mTransform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Sect.transform(new FissionCallback<TransformInfo>() {
                    @Override
                    public void onSuccess(TransformInfo transformInfo) {
                        if (transformInfo != null) {
                            ToastUtil.showToast(TongActivity.this, "进行现金兑换成功");
                            Log.d(TAG, transformInfo.toString());
                        } else {
                            ToastUtil.showToast(TongActivity.this, "进行现金兑换为空");
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                        ToastUtil.showToast(TongActivity.this, "进行现金兑换失败： " + msg);
                    }
                });
            }
        });

    }
}
