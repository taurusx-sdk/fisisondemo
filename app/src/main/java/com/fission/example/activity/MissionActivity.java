package com.fission.example.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.fission.account.FissionAccounts;
import com.fission.example.R;
import com.fission.example.constance.Constance;
import com.fission.sdk.Fission;
import com.fission.sdk.FissionCallback;
import com.fission.sdk.bean.task.RecentDaysTaskBean;
import com.fission.sdk.bean.task.TaskCoinsBean;
import com.fission.sdk.bean.task.TaskConfigBean;
import com.fission.sdk.bean.task.TaskCountBean;
import com.fission.sdk.bean.task.TaskRewardedBean;

import java.util.List;

public class MissionActivity extends BaseActivity {
    private static final String TAG = Constance.TAG;
    // task
    private static Button mQueryAllTasks;
    private static Button mSubmitTaskButton;
    private static Button mDoubleRewardButton;
    private static Button mQueryTaskRecordsButton;
    private static Button mQuerySubmitCountsTasks;
    private static Button mQueryTodayCoinsTasks;
    private static Button mQueryInviteeTasks;
    private static Button mQueryInviteeSubmitRecords;
    private static Button mQueryInviteeTaskRecords;

    private static String mRecordId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_misison);
        initView();
    }

    private void initView() {

        mQueryAllTasks = findViewById(R.id.fission_demo_query_all_tasks);
        mQueryAllTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(MissionActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fission.Task.queryAllTasks(new FissionCallback<List<TaskConfigBean>>() {
                    @Override
                    public void onSuccess(List<TaskConfigBean> taskConfigs) {
                        if (taskConfigs != null && taskConfigs.size() > 0) {
                            for (TaskConfigBean config : taskConfigs) {
                                Log.d(TAG, "the task config is :" + config);
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mSubmitTaskButton = findViewById(R.id.fission_demo_submit_task);
        mSubmitTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(MissionActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fission.Task.startSubmitTask("random_redpack", 50, 0, new FissionCallback<TaskRewardedBean>() {
                    @Override
                    public void onSuccess(TaskRewardedBean submitTaskData) {
                        Log.d(TAG, "the response is :" + submitTaskData);
                        mRecordId = submitTaskData.getRecord().getId();
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mDoubleRewardButton = findViewById(R.id.fission_demo_submit_double);
        mDoubleRewardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fission.Task.startMultiplyTask("random_redpack", mRecordId, 2, new FissionCallback<TaskRewardedBean>() {
                    @Override
                    public void onSuccess(TaskRewardedBean submitTaskData) {
                        Log.d(TAG, "the response is :" + submitTaskData);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mQueryTaskRecordsButton = findViewById(R.id.fission_demo_query_task);
        mQueryTaskRecordsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(MissionActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fission.Task.startQueryRecentDayTasks( "random_redpack", 1, -1, -1, new FissionCallback<RecentDaysTaskBean>() {
                    @Override
                    public void onSuccess(RecentDaysTaskBean taskRecords) {
                        Log.d(TAG, "the response is :" + taskRecords);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mQuerySubmitCountsTasks = findViewById(R.id.fission_demo_query_submit_count);
        mQuerySubmitCountsTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(MissionActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fission.Task.startQueryTaskCountInfo("random_redpack", 1, new FissionCallback<TaskCountBean>() {
                    @Override
                    public void onSuccess(TaskCountBean taskRecords) {
                        Log.d(TAG, "the response is :" + taskRecords);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mQueryTodayCoinsTasks = findViewById(R.id.fission_demo_query_today_coins);
        mQueryTodayCoinsTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(MissionActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fission.Task.startGetTodayCoins( new FissionCallback<TaskCoinsBean>() {
                    @Override
                    public void onSuccess(TaskCoinsBean taskRecords) {
                        Log.d(TAG, "the response is :" + taskRecords);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });
    }
}
