package com.fission.example.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.fission.account.Callback;
import com.fission.account.FissionAccountType;
import com.fission.account.FissionAccounts;
import com.fission.account.bean.SpecificUserInfo;
import com.fission.account.bean.UserBean;
import com.fission.account.bean.UserTokenBean;
import com.fission.example.R;
import com.fission.example.login.FacebookLogin;
import com.fission.example.login.GoogleLogin;
import com.fission.example.login.UserInfo;
import com.we.modoo.ModooHelper;
import com.we.modoo.callback.LoginCallback;
import com.we.modoo.core.LoginType;

import java.util.ArrayList;
import java.util.List;

public class UserActivity extends BaseActivity {
    private static final String TAG = "FissionDemo";

    private static String mWXCode;
    private static String mWXAppId = "wxf4443d19c4266f6b";
    private static String mSourceId = "";

    private static String mFBToken;
    private static String mFBOpenId;

    private static String mGoogleToken;
    // user
    private static Button mVisitorButton;
    private static Button mWXLoginButton;
    private static Button mFBLoginButton;
    private static Button mGoogleLoginButton;
    private static Button mWXRegisterButton;
    private static Button mFBRegisterButton;
    private static Button mGoogleRegisterButton;
    private static Button mAccountCheckButton;
    private static Button mAccountBindButton;

    private static Button mSyncWechatButton;

    private static Button mGetSpecialUserButton;
    private static Button mGetInviterButton;
    private static Button mGetTokenButton;


    private static Button mGetUserButton;
    private static Button mGetUserRanking;
    private static Button mUnregisterButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        initView();
    }

    private void initView() {
        mVisitorButton = findViewById(R.id.fission_demo_register_vistor);
        mVisitorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(UserActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                FissionAccounts.registerVisitor(0, mSourceId, new Callback<UserBean>() {
                    @Override
                    public void onSuccess(UserBean user) {
                        Log.d(TAG, "the response is :" + user.toString());
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mWXLoginButton = findViewById(R.id.fission_demo_wx_login);
        mWXLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModooHelper.setLoginCallback(new LoginCallback() {
                    @Override
                    public void loginSuccess(String wxCode) {
                        Log.d(TAG, "wx login success and the code is " + wxCode);
                        mWXCode = wxCode;
                    }

                    @Override
                    public void loginCancel(String s) {
                        Log.d(TAG, "wx login cancel");
                    }

                    @Override
                    public void loginFailed(String s) {
                        Log.d(TAG, "wx login failed");
                    }
                });
                ModooHelper.login(LoginType.Wechat);
            }
        });

        mFBLoginButton = findViewById(R.id.fission_demo_fb_login);
        mFBLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FacebookLogin.facebookLogin(UserActivity.this, new com.fission.example.login.LoginCallback() {
                    @Override
                    public void onSuccess(UserInfo user) {
                        if (user != null) {
                            mFBOpenId = user.mUserId;
                            mFBToken = user.mToken;
                        }
                    }

                    @Override
                    public void onFailed(String msg) {

                    }
                });
            }
        });

        mGoogleLoginButton = findViewById(R.id.fission_demo_google_login);
        mGoogleLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoogleLogin.googleLogin(UserActivity.this, new com.fission.example.login.LoginCallback() {
                    @Override
                    public void onSuccess(UserInfo user) {
                        if (user != null) {
                            mGoogleToken = user.mToken;
                        }
                    }

                    @Override
                    public void onFailed(String msg) {

                    }
                });
            }
        });

        mWXRegisterButton = findViewById(R.id.fission_demo_wx_register);
        mWXRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(UserActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(mWXCode)) {
                    Toast.makeText(UserActivity.this, "wx code is null, please login wx first", Toast.LENGTH_SHORT).show();
                    return;
                }
                FissionAccounts.registerWithWechat(mWXAppId, mWXCode, mSourceId, new Callback<UserBean>() {
                    @Override
                    public void onSuccess(UserBean user) {
                        Log.d(TAG, "the response is :" + user.toString());
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mFBRegisterButton = findViewById(R.id.fission_demo_fb_register);
        mFBRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(UserActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(mFBOpenId) || TextUtils.isEmpty(mFBToken)) {
                    Toast.makeText(UserActivity.this, "wx code is null, please login google first", Toast.LENGTH_SHORT).show();
                    return;
                }
                FissionAccounts.registerWithFacebook(UserActivity.this, mFBOpenId, mFBToken, System.currentTimeMillis(), new Callback<UserBean>() {
                    @Override
                    public void onSuccess(UserBean user) {
                        Log.d(TAG, "the response is :" + user.toString());
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mGoogleRegisterButton = findViewById(R.id.fission_demo_google_register);
        mGoogleRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(UserActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(mGoogleToken)) {
                    Toast.makeText(UserActivity.this, "wx code is null, please login google first", Toast.LENGTH_SHORT).show();
                    return;
                }
                FissionAccounts.registerWithGoogle(UserActivity.this, mGoogleToken, System.currentTimeMillis(), new Callback<UserBean>() {
                    @Override
                    public void onSuccess(UserBean user) {
                        Log.d(TAG, "the response is :" + user.toString());
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });


        mAccountBindButton = findViewById(R.id.fission_demo_bind_account);
        mAccountBindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(UserActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(mWXCode)) {
                    Toast.makeText(UserActivity.this, "wx code is null, please login wx first", Toast.LENGTH_SHORT).show();
                    return;
                }
                FissionAccounts.startBindAccount(FissionAccountType.TYPE_WECHAT, mWXAppId, mWXCode, new Callback<UserBean>() {
                    @Override
                    public void onSuccess(UserBean user) {
                        Log.d(TAG, "the response is :" + user);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mGetSpecialUserButton = findViewById(R.id.fission_demo_get_special_user);
        mGetSpecialUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(UserActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<String> userList = new ArrayList<>();
                userList.add("6823c6e402f8b373");
                FissionAccounts.getSpecificUsersInfo(userList, new Callback<List<SpecificUserInfo>>() {
                    @Override
                    public void onSuccess(List<SpecificUserInfo> specificUserInfos) {
                        if (specificUserInfos != null && specificUserInfos.size() > 0) {
                            for (SpecificUserInfo user : specificUserInfos) {
                                Log.d(TAG, "the response is :" + user);
                            }
                        }
                    }

                    @Override
                    public void onFailed(int i, String s) {

                    }
                });
            }
        });


        mGetTokenButton = findViewById(R.id.fission_demo_get_token_info);
        mGetTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(UserActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                FissionAccounts.getUserToken(new Callback<UserTokenBean>() {
                    @Override
                    public void onSuccess(UserTokenBean tokenInfo) {
                        Log.d(TAG, "the response is :" + tokenInfo);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mGetUserRanking = findViewById(R.id.fission_demo_get_user_ranking);
        mGetUserRanking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(UserActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                FissionAccounts.getUserRanking(5, 0, "coin", new Callback<List<UserBean>>() {
                    @Override
                    public void onSuccess(List<UserBean> users) {
                        if (users != null && users.size() > 0) {
                            for (UserBean user : users) {
                                Log.d(TAG, "the response is :" + user);
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mGetUserButton = findViewById(R.id.fission_demo_get_user_info);
        mGetUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(UserActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                FissionAccounts.getUserInfo(new Callback<UserBean>() {
                    @Override
                    public void onSuccess(UserBean user) {
                        Log.d(TAG, "the response is :" + user);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mUnregisterButton = findViewById(R.id.fission_demo_unregister);
        mUnregisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FissionAccounts.logout(new Callback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean success) {
                        Log.d(TAG, "the response is :" + success);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        GoogleLogin.onActivityResult(requestCode, resultCode, data);
        FacebookLogin.onActivityResult(requestCode, resultCode, data);
    }
}
