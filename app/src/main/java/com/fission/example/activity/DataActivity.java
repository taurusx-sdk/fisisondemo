package com.fission.example.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.fission.account.FissionAccounts;
import com.fission.example.R;
import com.fission.example.constance.Constance;
import com.fission.sdk.Fission;
import com.fission.sdk.FissionCallback;

public class DataActivity extends BaseActivity {
    private static final String TAG = Constance.TAG;
    // data
    private static Button mSaveDataButton;
    private static Button mQueryDataButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        initView();
    }

    private void initView() {
        mSaveDataButton = findViewById(R.id.fission_demo_save_user_data);
        mSaveDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(DataActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fission.Data.startStoreUserKV2Data("grade", "777", new FissionCallback<Boolean>() {

                    @Override
                    public void onSuccess(Boolean response) {
                        Log.d(TAG, "the response is :" + response);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });

            }
        });

        mQueryDataButton = findViewById(R.id.fission_demo_query_user_data);
        mQueryDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(DataActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fission.Data.startGetUserKVData2( "grade", new FissionCallback<String>() {
                    @Override
                    public void onSuccess(String s) {
                        Log.d(TAG, "the response is :" + s);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });
    }

}
