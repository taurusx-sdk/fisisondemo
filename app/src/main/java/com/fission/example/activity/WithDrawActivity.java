package com.fission.example.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.fission.account.FissionAccounts;
import com.fission.example.R;
import com.fission.example.constance.Constance;
import com.fission.sdk.Fission;
import com.fission.sdk.FissionCallback;
import com.fission.sdk.bean.withdraw.WithdrawBean;
import com.fission.sdk.bean.withdraw.WithdrawMissionsBean;

public class WithDrawActivity extends BaseActivity {
    private static final String TAG = Constance.TAG;

    private static String mRealName = "张松顺";
    //    private static String mContact = "13770561390";
//    private static String mComment = "test";
//    private static String mAddress = "nanjing jiangsu";
    private static String mIdCard = "320911198512011934";
    // withdraw
    private static Button mWXSmallWithdrawButton;
    private static Button mWXBigWithdrawButton;
    private static Button mQueryWithdrawButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);
        initView();
    }

    private void initView() {
        mWXSmallWithdrawButton = findViewById(R.id.fission_demo_wx_small_withdraw);
        mWXSmallWithdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(WithDrawActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fission.Withdraw.startRequestWXWithdraw("", "", new FissionCallback<WithdrawBean>() {
                    @Override
                    public void onSuccess(WithdrawBean withdrawBean) {
                        Log.d(TAG, "the response is :" + withdrawBean.toString());
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });

        mWXBigWithdrawButton = findViewById(R.id.fission_demo_wx_big_withdraw);
        mWXBigWithdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(WithDrawActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fission.Withdraw.startRequestWithdraw("", mRealName, mIdCard, null, null, new FissionCallback<WithdrawBean>() {
                    @Override
                    public void onSuccess(WithdrawBean withdrawBean) {
                        Log.d(TAG, "the response is :" + withdrawBean.toString());
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });


        mQueryWithdrawButton = findViewById(R.id.fission_demo_wx_query_withdraw);
        mQueryWithdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FissionAccounts.hasInitiated()) {
                    Toast.makeText(WithDrawActivity.this, "Not init, please init first", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fission.Withdraw.startQueryWithdraw(new FissionCallback<WithdrawMissionsBean>() {
                    @Override
                    public void onSuccess(WithdrawMissionsBean withdrawMissionsBean) {
                        Log.d(TAG, "the response is :" + withdrawMissionsBean.toString());
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        Log.d(TAG, "code is " + code + " msg: " + msg);
                    }
                });
            }
        });
    }
}
